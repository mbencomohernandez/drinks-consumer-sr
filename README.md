# Documentation

## Backend - AWS LAMBDA

### Build steps:

1. `npm install`
2. Copy `src/resource/config.ts.dist` to `src/resource/config.ts` and add values for any config.
3. `npm run build`


### Running tests steps:

1. `npm run test` will run all integration and unit tests in project
2. `npm run test-unit` will run unit tests only
3. `npm run test-integration` will run integration tests only

## Connecting back end to front end and run react 

### Build steps:
2. `cd client/ && npm install` installs all react dependencies
1. `npm start-dev` brings up express server and api endpoint: 'http://localhost:5000/'
2. `cd client/ && npm start`brings up react app: 'http://localhost:3000/'
