import {APIGatewayEvent, Callback, Context} from 'aws-lambda';
import {config} from '../resource/config';
import * as DynamoDB from 'aws-sdk/clients/dynamodb';
import {EventsTableDefinition} from '../port/aws/dynamodb/events-table-definition';
import * as Bluebird from 'bluebird';
import * as Logger from 'bunyan';
import {handleEvent} from '../action/handle-event';
import {retrieveEventData} from '../port/aws/dynamodb/events/retrieve-event-data';
import {interpretResultItemIntoEventObject} from '../port/aws/dynamodb/events/interpret-result-item-into-event-object';
import {interpretApiGatewayEvent} from '../port/api/interpret/interpret-api-gateway-event';

export const handle = (event: APIGatewayEvent, context: Context, callback: Callback) => {
    const logger = Logger.createLogger({
        name: config.app.name,
        requestId: context.awsRequestId,
    });

    logger.info({}, 'lambda-initiated:events');

    return Bluebird.try(() => {
        const dynamoClient = new DynamoDB.DocumentClient({
            region: config.aws.dynamoDb.region,
            apiVersion: config.aws.dynamoDb.apiVersion,
        });

        const eventsTableDefinition = new EventsTableDefinition(
            config.aws.dynamoDb.events.tableName,
            config.aws.dynamoDb.events.partitionKey,
        );

        const preparedRetrieveEventData = retrieveEventData({
            client: dynamoClient,
            tableDefinition: eventsTableDefinition,
            interpretResultItemIntoEventObject: interpretResultItemIntoEventObject
        });

        return handleEvent({
            interpretApiGatewayEvent: interpretApiGatewayEvent,
            retrieveEventData: preparedRetrieveEventData,
        })(event);
    }).then((responseBody) => {
        logger.info({}, 'lambda-finished:events');

        callback(null, {statusCode: 200, body: responseBody});
    }).catch((err) => {
        callback(err);
    });
};
