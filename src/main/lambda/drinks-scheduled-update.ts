import {Callback, Context, ScheduledEvent} from 'aws-lambda';
import {config} from '../resource/config';
import * as DynamoDB from 'aws-sdk/clients/dynamodb';
import {EventsTableDefinition} from '../port/aws/dynamodb/events-table-definition';
import * as Bluebird from 'bluebird';
import {storeEvents} from '../action/store-events';
import {httpsClient} from '../port/api/https-client';
import {addEventToEventsTable} from '../port/aws/dynamodb/events/add-event-to-events-table';
import * as moment from 'moment';
import * as Logger from 'bunyan';
import {interpretObjectIntoEvent} from '../port/api/interpret/interpret-object-into-event';

export const handle = (scheduledEvent: ScheduledEvent, context: Context, callback: Callback) => {
    const logger = Logger.createLogger({
        name: config.app.name,
        requestId: context.awsRequestId,
    });

    logger.info({}, 'lambda-initiated:drinks-scheduled-update');

    return Bluebird.try(() => {
        const dynamoClient = new DynamoDB.DocumentClient({
            region: config.aws.dynamoDb.region,
            apiVersion: config.aws.dynamoDb.apiVersion,
        });

        const eventsTableDefinition = new EventsTableDefinition(
            config.aws.dynamoDb.events.tableName,
            config.aws.dynamoDb.events.partitionKey,
        );

        const preparedHttpsClient = httpsClient({
            baseUrl: config.drinks.baseUrl,
        })(config.drinks.eventsEndpoint);

        const preparedAddEventsToTable = addEventToEventsTable({
            tableDefinition: eventsTableDefinition,
            client: dynamoClient,
            currentDatetime: moment(),
        });

        return storeEvents({
            listEventsFromDrinkApi: preparedHttpsClient,
            interpretObjectIntoEvent: interpretObjectIntoEvent,
            addEventToEventsTable: preparedAddEventsToTable,
        });
    }).then(() => {
        logger.info({}, 'lambda-finished:drinks-scheduled-update');

        callback(null, {statusCode: 200, body: {}});
    }).catch((err) => {
        callback(err);
    });
};
