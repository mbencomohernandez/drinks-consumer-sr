import {Event, EventComment, EventLocation, User} from '../../../../value/events';
import * as Bluebird from 'bluebird';
import {DocumentClient} from 'aws-sdk/lib/dynamodb/document_client';

export function interpretResultItemIntoEventObject(event: DocumentClient.AttributeMap): Bluebird<Event> {
    return Bluebird.try(() => {
        const creator = JSON.parse(event['creator']);
        const guests = JSON.parse(event['guests']);
        const location = JSON.parse(event['location']);
        const comments = JSON.parse(event['comments']);

        return new Event(
            event['id'],
            event['time'],
            event['title'],
            new User(
                creator['name'],
                creator['avatarUrl'],
            ),
            guests.map((guest: object) => {
                return new User(
                    guest['name'],
                    guest['avatarUrl'],
                );
            }),
            event['type'],
            new EventLocation(
                location['name'],
                location['latitude'],
                location['longitude'],
            ),
            comments.map((comment: object) => {
                return new EventComment(
                    new User(
                        comment['user']['name'],
                        comment['user']['avatarUrl'],
                    ),
                    comment['timestamp'],
                    comment['message'],
                );
            }),
        );
    });
}
