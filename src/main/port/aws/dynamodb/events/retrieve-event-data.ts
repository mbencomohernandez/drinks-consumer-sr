import {DocumentClient} from 'aws-sdk/lib/dynamodb/document_client';
import * as Bluebird from 'bluebird';
import {EventsTableDefinition} from '../events-table-definition';
import {Event} from '../../../../value/events';

export interface RetrieveEventDataDependency {
    client: DocumentClient
    tableDefinition: EventsTableDefinition
    interpretResultItemIntoEventObject: (event: DocumentClient.AttributeMap) => Bluebird<Event>
}

export const retrieveEventData = (dependency: RetrieveEventDataDependency) => {
    return (id: number): Bluebird<Event | null> => {
        return Bluebird.resolve(dependency.client.get({
            TableName: dependency.tableDefinition.tableName,
            Key: {
                [dependency.tableDefinition.partitionKey]: id,
            },
        }).promise()).then((result: DocumentClient.GetItemOutput) => {
            if (result.Item !== undefined) {
                return dependency.interpretResultItemIntoEventObject(result.Item);
            } else {
                return null;
            }
        });
    };
};
