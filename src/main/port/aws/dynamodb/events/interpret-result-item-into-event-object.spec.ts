import * as chai from 'chai';
import * as chaiAsPromise from 'chai-as-promised';
import {Event, EventComment, EventLocation, User} from '../../../../value/events';
import {DocumentClient} from 'aws-sdk/lib/dynamodb/document_client';
import {interpretResultItemIntoEventObject} from './interpret-result-item-into-event-object';

chai.use(chaiAsPromise);

const expect = chai.expect;

describe('port.aws.dynamodb.events.interpret-result-item-into-event-object', () => {
    it('it returns an event object whenn a result item from events table is passed', () => {
        const resultItem: DocumentClient.AttributeMap = {
            id: 1,
            time: '2017-06-23T20:00:00.000Z',
            title: 'James Bond Summer Ball',
            creator: JSON.stringify(
                {
                    'name': 'Phil Hardy',
                    'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                },
            ),
            guests: JSON.stringify(
                [
                    {
                        'name': 'Phil Hardy',
                        'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                    },
                    {
                        'name': 'Roger Planes',
                        'avatarUrl': 'https://graph.facebook.com/10153470472875756/picture?type=normal',
                    },
                ],
            ),
            type: 'COCKTAILS',
            location: JSON.stringify({
                'name': 'Radisson Blu Hotel',
                'latitude': 51.517608,
                'longitude': -0.127625,
            }),
            comments: JSON.stringify([
                {
                    'user': {
                        'name': 'Phil Hardy',
                        'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                    },
                    'timestamp': '2017-06-20T15:21:21.000Z',
                    'message': 'Looking forward to this!',
                },
                {
                    'user': {
                        'name': 'Roger Planes',
                        'avatarUrl': 'https://graph.facebook.com/10153470472875756/picture?type=normal',
                    },
                    'timestamp': '2017-06-23T18:53:45.000Z',
                    'message': 'Not long to go now :)',
                },
                {
                    'user': {
                        'name': 'Phil Hardy',
                        'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                    },
                    'timestamp': '2017-06-24T13:31:01.000Z',
                    'message': 'Thanks for coming everyone',
                },
            ])

        };

        const expected: Event = new Event(
            1,
            '2017-06-23T20:00:00.000Z',
            'James Bond Summer Ball',
            new User('Phil Hardy', 'https://graph.facebook.com/10154445192311988/picture?type=normal'),
            [
                new User('Phil Hardy', 'https://graph.facebook.com/10154445192311988/picture?type=normal'),
                new User('Roger Planes', 'https://graph.facebook.com/10153470472875756/picture?type=normal'),
            ],
            'COCKTAILS',
            new EventLocation('Radisson Blu Hotel', 51.517608, -0.127625),
            [
                new EventComment(
                    new User('Phil Hardy', 'https://graph.facebook.com/10154445192311988/picture?type=normal'),
                    '2017-06-20T15:21:21.000Z',
                    'Looking forward to this!',
                ),
                new EventComment(
                    new User('Roger Planes', 'https://graph.facebook.com/10153470472875756/picture?type=normal'),
                    '2017-06-23T18:53:45.000Z',
                    'Not long to go now :)',
                ),
                new EventComment(
                    new User('Phil Hardy', 'https://graph.facebook.com/10154445192311988/picture?type=normal'),
                    '2017-06-24T13:31:01.000Z',
                    'Thanks for coming everyone',
                ),
            ],
        );

        return expect(interpretResultItemIntoEventObject(resultItem)).to.eventually.deep.equal(expected);
    });
});