import {DocumentClient} from 'aws-sdk/lib/dynamodb/document_client';
import * as Bluebird from 'bluebird';
import {Moment} from 'moment';
import {EventsTableDefinition} from '../events-table-definition';
import {Event} from '../../../../value/events';

export interface AddEventToEventsTableDependency {
    client: DocumentClient
    tableDefinition: EventsTableDefinition
    currentDatetime: Moment
}

export const addEventToEventsTable = (dependency: AddEventToEventsTableDependency) => {
    return (event: Event) => {
        return Bluebird.resolve(dependency.client.put({
            TableName: dependency.tableDefinition.tableName,
            Item: {
                [dependency.tableDefinition.partitionKey]: event.id,
                time: event.time,
                title: event.title,
                creator: JSON.stringify(event.creator),
                guests: JSON.stringify(event.guests),
                type: event.type,
                location: JSON.stringify(event.location),
                comments: JSON.stringify(event.comments),
                updatedAt: dependency.currentDatetime.toISOString(),
                version: 1,
            },
        }).promise()).then(() => null);
    };
};
