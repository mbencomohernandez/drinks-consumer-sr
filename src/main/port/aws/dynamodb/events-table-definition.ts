export class EventsTableDefinition {
    constructor(
        readonly tableName: string,
        readonly partitionKey: string,
    ) {
    }
}
