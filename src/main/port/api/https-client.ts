import * as axios from 'axios';
import {AxiosError, AxiosRequestConfig} from 'axios';
import * as https from 'https';
import * as Bluebird from 'bluebird';

export interface HttpsClientDependency {
    baseUrl: string;
}

export function httpsClient(dependency: HttpsClientDependency) {
    return (resourceUri: string) => {
        return {
            get: (queryParameters: object): Bluebird<object[]> => {
                const uri: string = buildUrl(dependency.baseUrl, resourceUri);
                const httpsAgent = new https.Agent();
                const options: AxiosRequestConfig = {
                    httpsAgent: httpsAgent,
                    headers: {
                        Accept: 'application/json',
                    },
                    params: queryParameters,
                };

                return Bluebird.resolve(axios.default.get(uri, options)).then((responseBody) => {
                    return responseBody.data;
                }).catch((err) => {
                    throw new Error(err.message);
                });
            },

            // NOT IN USE
            post: (body: object): Bluebird<object> => {
                const uri: string = buildUrl(dependency.baseUrl, resourceUri);
                const httpsAgent = new https.Agent();

                const options: AxiosRequestConfig = {
                    httpsAgent: httpsAgent,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                };

                return Bluebird.resolve(axios.default.post(uri, body, options)).then((response) => {
                    return response.data;
                }).catch((err: AxiosError) => {
                    throw new Error(err.message);
                });
            },
        };
    };
}

function buildUrl(base: string, path: string) {
    let fixedBase = base;
    if (base.endsWith('/')) {
        fixedBase = base.slice(0, -1);
    }

    if (path.startsWith('/')) {
        return `${fixedBase}${path}`;
    } else {
        return `${fixedBase}/${path}`;
    }
}
