import {APIGatewayEvent} from 'aws-lambda';
import * as Bluebird from 'bluebird';
import * as Validator from 'ajv';
import {ErrorObject} from 'ajv';
import {apiGatewaySchema} from '../../../resource/schema/api-gateway-schema';
import {requestBodySchema} from '../../../resource/schema/request-body-schema';

export class InvalidRequestBodyError extends Error {
    constructor(readonly requestBody: object, readonly validationErrors: ErrorObject[] | undefined | null) {
        super('Validation of request body failed');
    }
}

export class InvalidApiGatewayEventError extends Error {
    constructor() {
        super('Api Gateway Event is not valid');
    }
}

const validator = new Validator({
    allErrors: true,
});

export function interpretApiGatewayEvent(event: APIGatewayEvent) {
    return Bluebird.try(() => {
        const apiGatewaySchemaValidator = validator.compile(apiGatewaySchema);
        const isValid = apiGatewaySchemaValidator(event);

        if (isValid && event.body !== null) {
            const requestBody: object = JSON.parse(event.body);
            const requestBodyValidator = validator.compile(requestBodySchema);
            const isRequestBodyValid = requestBodyValidator(requestBody);

            if (isRequestBodyValid) {
                return requestBody
            } else {
                throw new InvalidRequestBodyError(requestBody, requestBodyValidator.errors);
            }
        } else {
            throw new InvalidApiGatewayEventError();
        }
    });
}