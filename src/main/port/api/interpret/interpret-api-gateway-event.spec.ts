import * as chai from 'chai';
import * as chaiAsPromise from 'chai-as-promised';
import {
    interpretApiGatewayEvent,
    InvalidApiGatewayEventError,
    InvalidRequestBodyError,
} from './interpret-api-gateway-event';

chai.use(chaiAsPromise);

const expect = chai.expect;

describe('port.api.interpret.interpret-api-gateway-event', () => {
    it('returns body when validation passes', () => {
        it('returns InvalidRequestError when body does not contain ID', () => {
            const body: object = {id: 1};
            const event: any = {
                headers: {},
                httpMethod: 'POST',
                isBase64Encoded: true,
                path: '',
                pathParameters: null,
                queryStringParameters: null,
                stageVariables: null,
                requestContext: null,
                resource: null,
                body: JSON.stringify(body),
            };

            return expect(interpretApiGatewayEvent(event)).to.eventually.deep.equals({id: 1});
        });
    });

    it('throws InvalidRequestError when body does not contain ID', () => {
        const body: object = {};
        const event: any = {
            headers: {},
            httpMethod: 'POST',
            isBase64Encoded: true,
            path: '',
            pathParameters: null,
            queryStringParameters: null,
            stageVariables: null,
            requestContext: null,
            resource: null,
            body: JSON.stringify(body),
        };

        return expect(interpretApiGatewayEvent(event)).to.be.rejectedWith(InvalidRequestBodyError);
    });

    it('throws InvalidApiGatewayEventError when event doesnt contain body', () => {
        const event: any = {
            headers: {},
            httpMethod: 'POST',
            isBase64Encoded: true,
            path: '',
            pathParameters: null,
            queryStringParameters: null,
            stageVariables: null,
            requestContext: null,
            resource: null,
        };

        return expect(interpretApiGatewayEvent(event)).to.be.rejectedWith(InvalidApiGatewayEventError);
    });
});