import {Event, User, EventLocation, EventComment} from '../../../value/events';
import * as Bluebird from 'bluebird';

export function interpretObjectIntoEvent(event: object): Bluebird<Event> {
    return Bluebird.try(() => {
        return new Event(
            event['id'],
            event['time'],
            event['title'],
            new User(
                event['creator']['name'],
                event['creator']['avatarUrl'],
            ),
            event['guests'].map((guest: object) => {
                return new User(
                    guest['name'],
                    guest['avatarUrl'],
                );
            }),
            event['type'],
            new EventLocation(
                event['location']['name'],
                event['location']['latitude'],
                event['location']['longitude'],
            ),
            event['comments'].map((comment: object) => {
                return new EventComment(
                    new User(
                        comment['user']['name'],
                        comment['user']['avatarUrl'],
                    ),
                    comment['timestamp'],
                    comment['message'],
                );
            }),
        );
    });
}
