import * as chai from 'chai';
import * as chaiAsPromise from 'chai-as-promised';
import {httpsClient, HttpsClientDependency} from './https-client';
import * as nock from 'nock';

chai.use(chaiAsPromise);

const expect = chai.expect;

describe('port.api.https-client', () => {
    const httpsClientDependency: HttpsClientDependency = {
        baseUrl: 'https://mock-api.drinks.test.siliconrhino.io',
    };

    it('correct response is returned on get request', () => {
        const body = [
            {
                'id': 1,
                'type': 'COCKTAILS',
                'time': '2017-06-23T20:00:00.000Z',
                'title': 'James Bond Summer Ball',
                'location': {
                    'name': 'Radisson Blu Hotel',
                    'latitude': 51.517608,
                    'longitude': -0.127625,
                },
                'creator': {
                    'name': 'Phil Hardy',
                    'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                },
                'guests': [
                    {
                        'name': 'Phil Hardy',
                        'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                    },
                    {
                        'name': 'Roger Planes',
                        'avatarUrl': 'https://graph.facebook.com/10153470472875756/picture?type=normal',
                    },
                ],
                'comments': [
                    {
                        'user': {
                            'name': 'Phil Hardy',
                            'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                        },
                        'timestamp': '2017-06-20T15:21:21.000Z',
                        'message': 'Looking forward to this!',
                    },
                    {
                        'user': {
                            'name': 'Roger Planes',
                            'avatarUrl': 'https://graph.facebook.com/10153470472875756/picture?type=normal',
                        },
                        'timestamp': '2017-06-23T18:53:45.000Z',
                        'message': 'Not long to go now :)',
                    },
                    {
                        'user': {
                            'name': 'Phil Hardy',
                            'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                        },
                        'timestamp': '2017-06-24T13:31:01.000Z',
                        'message': 'Thanks for coming everyone',
                    },
                ],
            },
            {
                'id': 2,
                'type': 'COCKTAILS',
                'time': '2017-12-23T20:00:00.000Z',
                'title': 'James Bond Winter Ball',
                'location': {
                    'name': 'Radisson Blu Hotel',
                    'latitude': 51.517608,
                    'longitude': -0.127625,
                },
                'creator': {
                    'name': 'Mark Hartley',
                    'avatarUrl': 'https://graph.facebook.com/10207459310094409/picture?type=normal',
                },
                'guests': [
                    {
                        'name': 'Phil Hardy',
                        'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                    },
                ],
                'comments': [
                    {
                        'user': {
                            'name': 'Phil Hardy',
                            'avatarUrl': 'https://graph.facebook.com/10154445192311988/picture?type=normal',
                        },
                        'timestamp': '2017-06-24T14:32:27.000Z',
                        'message': 'Can you send details about the dress code?',
                    },
                ],
            },
        ];

        nock(httpsClientDependency.baseUrl)
            .get('/events?page=1&pageSize=2&search=Bond')
            .reply(200, body);

        const queryParameters = {
            page: 1,
            pageSize: 2,
            search: 'Bond',
        };

        return expect(httpsClient(httpsClientDependency)('/events').get(queryParameters)).to.eventually.deep.eq(body);
    });

    it('an error is thrown on error', () => {
        nock(httpsClientDependency.baseUrl)
            .get('/events')
            .replyWithError('Unexpected Error');

        const queryParameters = {};

        return expect(httpsClient(httpsClientDependency)('/events').get(queryParameters)).to.eventually.be.rejectedWith(Error);
    });

    it('correct response is returned on post request ', () => {
        nock(httpsClientDependency.baseUrl)
            .post('/event')
            .reply(200, {test: true});

        const requestBody = {};

        return expect(httpsClient(httpsClientDependency)('/event').post(requestBody)).to.eventually.deep.eq({test: true});
    });

    afterEach((done) => {
        nock.cleanAll();
        nock.enableNetConnect();

        done();
    });
});
