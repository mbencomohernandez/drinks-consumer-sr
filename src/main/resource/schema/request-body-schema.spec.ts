import * as chai from 'chai';
import * as Validator from 'ajv';
import {requestBodySchema} from './request-body-schema';

const expect = chai.expect;

describe('resource.schema.request-body-schema', () => {
    const validator = new Validator();

    const inputEvents = [
        {
            description: 'empty string',
            input: '',
            valid: false,
        },
        {
            description: 'empty object',
            input: {},
            valid: false,
        },
        {
            description: 'body contains id',
            input: {
                id: 2,
            },
            valid: true,
        }
    ];

    inputEvents.forEach((inputEvent) => {
        it(`input event: "${inputEvent.description}" is ${(inputEvent.valid) ? 'valid' : 'invalid'}`, () => {
            const validateFunction = validator.compile(requestBodySchema);

            expect(validateFunction(inputEvent.input)).to.deep.equal(inputEvent.valid);
        });
    });
});
