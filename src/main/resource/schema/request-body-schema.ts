export const requestBodySchema = {
    type: 'object',
    required: ['id'],
    additionalProperties: true,
    properties: {
        id: {
            type: 'number',
            minLength: 1,
        },
    },
};
