export const apiGatewaySchema = {
    type: 'object',
    required: ['headers', 'body'],
    additionalProperties: true,
    properties: {
        headers: {
            type: 'object',
        },
        body: {
            type: 'string',
            minLength: 1,
        },
    },
};
