import * as chai from 'chai';
import * as Validator from 'ajv';
import {apiGatewaySchema} from './api-gateway-schema';

const expect = chai.expect;

describe('resource.schema.api-gateway-schema', () => {
    const validator = new Validator();

    const inputEvents = [
        {
            description: 'empty string',
            input: '',
            valid: false,
        },
        {
            description: 'empty object',
            input: {},
            valid: false,
        },
        {
            description: 'headers and empty body present',
            input: {
                headers: {
                    'content': 'application/json'
                },
                body: {},
            },
            valid: false,
        },
        {
            description: 'only headers present',
            input: {
                headers: {
                    'content': 'application/json'
                },
            },
            valid: false,
        },
        {
            description: 'header present and empty body string',
            input: {
                headers: {
                    'content': 'application/json'
                },
                body: '',
            },
            valid: false,
        },
        {
            description: 'header and body string contains id',
            input: {
                headers: {
                    'content': 'application/json'
                },
                body: '{id: 2}',
            },
            valid: true,
        }
    ];

    inputEvents.forEach((inputEvent) => {
        it(`input event: "${inputEvent.description}" is ${(inputEvent.valid) ? 'valid' : 'invalid'}`, () => {
            const validateFunction = validator.compile(apiGatewaySchema);

            expect(validateFunction(inputEvent.input)).to.deep.equal(inputEvent.valid);
        });
    });
});
