export class Event {
    public constructor(
        readonly id: number,
        readonly time: string,
        readonly title: string,
        readonly creator: User,
        readonly guests: User[],
        readonly type: 'BEERS' | 'COCKTAILS' | 'COFFEES' | 'MILKSHAKES',
        readonly location: EventLocation,
        readonly comments: EventComment[],
    ) {}
}

export class EventLocation {
    public constructor(
        readonly name: string,
        readonly latitude: number,
        readonly longitude: number,
    ) {}
}

export class EventComment {
    public constructor(
        readonly user: User,
        readonly timestamp: string,
        readonly message: string,
    ) {}
}

export class User {
    public constructor(
        readonly name: string,
        readonly avatarUrl: string,
    ) {}
}
