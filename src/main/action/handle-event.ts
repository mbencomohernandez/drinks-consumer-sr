import {APIGatewayEvent} from 'aws-lambda';
import * as Bluebird from 'bluebird';
import {Event} from '../value/events';
import {interpretApiGatewayEvent} from '../port/api/interpret/interpret-api-gateway-event';
import {retrieveEventData} from '../port/aws/dynamodb/events/retrieve-event-data';

export interface HandleEventDependency {
    interpretApiGatewayEvent: (event: APIGatewayEvent) => Bluebird<object>
    retrieveEventData: (id: number) => Bluebird<Event | null>;
}

export function handleEvent(dependency: HandleEventDependency) {
    return (event: APIGatewayEvent) => {
        return interpretApiGatewayEvent(event).then((requestBody: object) => {
            return retrieveEventData(requestBody['id']);
        });
    };
}