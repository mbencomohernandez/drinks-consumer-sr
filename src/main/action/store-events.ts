import * as Bluebird from 'bluebird';
import {Event} from '../value/events';

export interface ListEventsFromDrinksApi {
    get: (queryParameters: object) => Bluebird<object[]>;
}

export interface StoreEventsDependency {
    listEventsFromDrinkApi: ListEventsFromDrinksApi;
    interpretObjectIntoEvent: (event: object) => Bluebird<Event>;
    addEventToEventsTable: (event: Event) => Bluebird<null>;
}

export const storeEvents = (dependency: StoreEventsDependency) => {
    const queryParameters = {};

    return dependency.listEventsFromDrinkApi.get(queryParameters).then((events) => {
        return Bluebird.each(events, (event: object) => {
            return dependency.interpretObjectIntoEvent(event).then((event: Event) => {
                return dependency.addEventToEventsTable(event);
            });
        });
    });
};
