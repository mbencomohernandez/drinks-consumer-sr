import {httpsClient} from './main/port/api/https-client';

const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

// @ts-ignore
app.get('/api/events', (req, res) => {
    httpsClient({
        baseUrl: 'https://mock-api.drinks.test.siliconrhino.io',
    })('events').get({}).then((events) => {
        res.send(events);
    });
});
