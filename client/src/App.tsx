import React, {useEffect, useState} from 'react';
import './App.css';
import {withListLoading} from './component/withListLoading';
import {List} from './component/List';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    const ListLoading = withListLoading(List);

    const [appState, setAppState] = useState({
        loading: false,
        events: null,
    });

    useEffect(() => {
        setAppState({loading: true, events: null});
        const apiUrl = `/api/events`;
        fetch(apiUrl)
            .then((res) => res.json())
            .then((events) => {
                setAppState({loading: false, events: events});
            });
    }, [setAppState]);

    return (
        <div className='App'>
            <div className='container'>
                <h1 className="App-header">Events</h1>
                <ListLoading isLoading={appState.loading} events={appState.events}/>
            </div>
        </div>
    );
}

export default App;