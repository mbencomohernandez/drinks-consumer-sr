import React from 'react';
import {Event} from '../../../src/main/value/events';

// @ts-ignore
export function List(props) {
    const {events} = props;
    if (!events || events.length === 0) return <p>No events, sorry</p>;

    return (
        <ul className="list-group">
            {events.map((event: Event) => {
                return (
                    <li className="list-group-item" key={event.id}>
                        <div className="list-group justify-content-lg-end">
                            <h4>{event.title}</h4>
                            <div>
                                <div className="card" style={{width: '100px'}}>
                                    <img className="card-img-top" src={event.creator.avatarUrl} alt="avatar"/>
                                    <p className="card-text"> {event.creator.name}</p>
                                </div>
                                <h6>{event.location.name}</h6>
                                <p>{new Date(event.time).toDateString() + ' ' + new Date(event.time).toTimeString()}</p>
                            </div>
                        </div>
                    </li>
                );
            })}
        </ul>
    );
}

