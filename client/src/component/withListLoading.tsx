import React from 'react';

//@ts-ignore
export function withListLoading(Component) {
    //@ts-ignore
    return function WihLoadingComponent({ isLoading, ...props }) {
        if (!isLoading) return <Component {...props} />;
        return (
            <p style={{ textAlign: 'center', fontSize: '30px' }}>
                LOADING...
            </p>
        );
    };
}